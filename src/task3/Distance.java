package task3;

import java.util.Scanner;

public class Distance {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("input distance(m): ");
        double distance = in.nextDouble();
        System.out.println("choose the unit of measurement:\n1 - miles\n2 - kilometers");
        int choice = in.nextInt();

        switch (choice) {
            case 1 -> {
                Distance dist = new Distance();
                dist.print(distance);
                System.out.println("miles: " + Converter.metersToMiles(distance));
            }
            case 2 -> {
                Distance dist2 = new Distance();
                dist2.print(distance);
                System.out.println("kilometers: " + Converter.metersToKilometers(distance));
            }
            default -> System.out.println("incorrect choice");
        }
        in.close();
    }

    void print(double distance) {
        System.out.println("meters: " + distance);
    }

    static class Converter {
        static double metersToMiles(double arg) {
            return arg * 0.00062;
        }

        static double metersToKilometers(double arg) {
            return arg * 0.001;
        }
    }
}
