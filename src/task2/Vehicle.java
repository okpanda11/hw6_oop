package task2;

public class Vehicle {

    public static void main(String[] args) {
        Vehicle.Door myDoor = new Vehicle().new Door();
        Vehicle.Wheel myWheel = new Vehicle().new Wheel();

        myDoor.print();
        myWheel.print();
    }

    // inner class
    protected class Wheel {
        void print() {
            System.out.println("print from Wheel");
        }
    }

    // inner class
    protected class Door {
        void print() {
            System.out.println("print from Door");
        }
    }

    //method from top class
    void print() {
        System.out.println("print from Vehicle");
    }
}
