package bonusTask;

import java.text.DecimalFormat;
import java.util.Scanner;

public class Calculator {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("input 1st arg:");
        double arg1 = sc.nextDouble();
        System.out.println("input 2nd arg:");
        double arg2 = sc.nextDouble();
        System.out.println("input operation sign( +, -, *, / ):");
        String sign = sc.next();

        //output format
        DecimalFormat df = new DecimalFormat("0.00");

        switch (sign) {
            case "+" -> {
                Calculator.Operator add = new Calculator().new Operator();
                System.out.println(arg1 + " + " + arg2 + " = " + df.format(add.addition(arg1, arg2)));
            }
            case "-" -> {
                Calculator.Operator sub = new Calculator().new Operator();
                System.out.println(arg1 + " - " + arg2 + " = " + df.format(sub.subtraction(arg1, arg2)));
            }
            case "*" -> {
                Calculator.Operator multi = new Calculator().new Operator();
                System.out.println(arg1 + " * " + arg2 + " = " + df.format(multi.multiplication(arg1, arg2)));
            }
            case "/" -> {
                Calculator.Operator div = new Calculator().new Operator();
                System.out.println(arg1 + " / " + arg2 + " = " + df.format(div.division(arg1, arg2)));
            }
            default -> System.out.println("incorrect choice");
        }

    }

    protected class Operator {

        double addition(double arg1, double arg2) {
            return arg1 + arg2;
        }

        double subtraction(double arg1, double arg2) {
            return arg1 - arg2;
        }

        double multiplication(double arg1, double arg2) {
            return arg1 * arg2;
        }

        double division(double arg1, double arg2) {
            if (arg2 == 0) {
                System.out.println("division by zero");
            }
            return arg1 / arg2;
        }
    }

}